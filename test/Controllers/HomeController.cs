﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Newtonsoft.Json.Linq;
using test.Models;

namespace test.Controllers
{
    public class HomeController : Controller
    {
        ValuesController vc = new ValuesController();

        public ActionResult Index()
        {
            return View(new List<Models.User>());
        }

        public ActionResult GetUsers()
        {
            List<User> userModel = vc.Users();
            return View("Index", userModel);
        }

        [HttpPost]
        public ActionResult GetUser(int id)
        {
            List<User> userList = vc.Users();
            List<User> us = new List<User>();
            try
            {
                us.Add(userList[id - 1]);
            }
            catch (Exception e)
            {
                return View("Index", us);
            }
            return View("Index", us);
        }

        
        public ActionResult Albums()
        {
            return View("Albums", new List<Albums>());
        }

        public ActionResult GetAllAlbums()
        {
            List<Albums> albumModel = vc.Albums();
            return View("Albums", albumModel);
        }

        [HttpPost]
        public ActionResult GetAlbum(int id)
        {
            List<Albums> albumModel = vc.Albums();
            List<Albums> us = new List<Albums>();
            try
            {
                us.Add(albumModel[id - 1]);
            }
            catch (Exception e)
            {
                return View("Albums", us);
            }
            return View("Albums", us);
        }

        [HttpPost]
        public ActionResult GetUsersAlbums(int id)
        {
            List<Albums> albumModel = vc.Users(id, "albums");
            try
            {
                return View("Albums", albumModel);
            }
            catch (Exception e)
            {
                return View("Albums", new List<Albums>());
            }
        }
    }
}
