﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using test.Models;

namespace test.Controllers
{
    public class ValuesController : ApiController
    {
        private readonly StringBuilder request = new StringBuilder("http://jsonplaceholder.typicode.com");
        
        // GET api/values/users
        [HttpGet]
        public List<User> Users()
        {
            return CreateListGetResponse<User>("/users");
        }

        // GET api/values/user/1
        [HttpGet]
        public User Users(int id)
        {
            string t = "/users/" + id;
            return CreateGetResponse<User>(t);
        }
        
        // GET api/values/user/1/albums
        [HttpGet]
        public List<Albums> Users(int id, string route)
        {
            string t = "/users/" + id + "/" + route;
            return CreateListGetResponse<Albums>(t);
        }
        
        // GET api/values/albums
        [HttpGet]
        public List<Albums> Albums()
        {
            return CreateListGetResponse<Albums>("/albums");
        }
        
        // GET api/values/albums/1
        [HttpGet]
        public Albums Albums(int id)
        {
            string t = "/albums/" + id;
            return CreateGetResponse<Albums>(t);
        }

        // POST api/values
        [HttpPost]
        public void CreateUser([FromBody]string user)
        {
        }

        // PUT api/values/5
        [HttpPut]
        public void EditUser(int id, [FromBody]string user)
        {
        }

        // DELETE api/values/5
        public void DeleteUser(int id)
        {
        }

        private List<T> CreateListGetResponse<T>(String appendString)
        {
            HttpWebRequest additionalRequest = (HttpWebRequest) WebRequest.Create(request.Append(appendString).ToString());
            using (WebResponse response = additionalRequest.GetResponse())
            {
                if (response.GetResponseStream() == null)
                    return new List<T>();

                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    List<T> list = JsonConvert.DeserializeObject<List<T>>(rd.ReadToEnd());
                    return list;
                }
            }
        }

        private T CreateGetResponse<T>(String appendString)
        {
            HttpWebRequest additionalRequest = (HttpWebRequest)WebRequest.Create(request.Append(appendString).ToString());
            using (WebResponse response = additionalRequest.GetResponse())
            {
                if (response.GetResponseStream() == null)
                    return default(T);

                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    T list = JsonConvert.DeserializeObject<T>(rd.ReadToEnd());
                    return list;
                }
            }
        }
    }
}
