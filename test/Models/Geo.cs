﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace test.Models
{
    public class Geo
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }
}